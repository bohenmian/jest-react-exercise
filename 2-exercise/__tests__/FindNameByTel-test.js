import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';
import FindNameByTel from '../FindNameByTel';

const setup = () => {
  const { getByLabelText, getByTestId } = render(<FindNameByTel />);
  const input = getByLabelText('tel');
  const name = getByTestId('name');
  return {
    input,
    name
  };
};

test('FindNameByTel组件通过输入手机号渲染姓名', () => {
  const { input, name } = setup();

  // <--start
  fireEvent.change(input, { target: { value: '15000000000' } });
  expect(name).toHaveTextContent('Tom');
  // --end->
});
