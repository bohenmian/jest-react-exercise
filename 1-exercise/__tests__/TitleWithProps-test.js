import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { cleanup, render } from '@testing-library/react';
import TitleWithProps from '../TitleWithProps';

// <--start
beforeEach(() => {
  expect.hasAssertions();
});
afterEach(cleanup);

// --end->

test('TitleWithProps组件渲染内容', () => {
  // <--start
  const { getByTestId } = render(<TitleWithProps name="World" />);
  expect(getByTestId('title')).toHaveTextContent('Hello World');
  // --end->
});
