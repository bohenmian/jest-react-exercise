import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { cleanup, render } from '@testing-library/react';
import TitleWithState from '../TitleWithState';

// <--start
beforeEach(() => {
  expect.hasAssertions();
});
afterEach(cleanup);
// --end->

test('TitleWithState组件渲染内容', () => {
  // <--start
  const { getByTestId } = render(<TitleWithState />);
  expect(getByTestId('title')).toHaveTextContent('Hello Silent World');
  // --end->
});
